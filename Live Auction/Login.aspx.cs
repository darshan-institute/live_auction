﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;

namespace Live_Auction
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();
        }

        protected void login_Click(object sender, EventArgs e)
        {
            if (txtusername.Text == "")
            {
                lblerror.Text = "Enter Username";
            }
            else if (txtpassword.Text == "")
            {
                lblerror.Text = "Enter Password";
            }
            else
            {
                String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                con.ConnectionString = constr;
                con.Open();
                string selectuser = "SELECT * FROM user_detail WHERE user_Name = '" + txtusername.Text.Trim() + "' AND user_password = '" + txtpassword.Text.Trim() + "'";
                SqlCommand cmd = new SqlCommand(selectuser, con);
                SqlDataReader dr = cmd.ExecuteReader();
                if (txtusername.Text == "admin")
                {
                    if (txtpassword.Text == "admin123")
                    {
                        Response.Redirect("admin.aspx");
                    }
                }
                else if (dr.HasRows == true)
                {
                    Session["UserName"] = txtusername.Text;
                    Response.Redirect("home.aspx");
                    Session.RemoveAll();
                }
                else
                {
                    string error = "Enter correct username or password";
                    ClientScript.RegisterStartupScript(this.GetType(), "", "alert('" + error + "');", true);
                    Session.RemoveAll();
                }
                txtusername.Text = " ";
                txtpassword.Text = " ";
            }
        }

        protected void signup_Click(object sender, EventArgs e)
        {
            Response.Redirect("signup.aspx");
        }
    }
}
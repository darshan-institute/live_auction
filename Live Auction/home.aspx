﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="home.aspx.cs" MasterPageFile="~/Masterpage.Master" Inherits="Live_Auction.admin" %>

<asp:Content ID="cnt0" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-left: 158px;
        }
    </style>
</asp:Content>

<asp:Content ID="cnt1" ContentPlaceHolderID="contentbody" runat="server">
    <asp:Label ID="lbldislay" runat="server" Text="Available Product" ForeColor="Purple" style="font-size:30px;" Font-Bold="true"></asp:Label>
   <div>


         <asp:DataList ID="DataList1" runat="server" OnItemCommand="DataList1_ItemCommand" BackColor="White"  BorderColor="#CC9966" BorderStyle="None"  BorderWidth="1px" CellPadding="4" DataSourceID="SqlDataSource1" GridLines="Both" style="width:75%;" CssClass="auto-style1" Width="610px" RepeatColumns="4">
        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
        <ItemStyle ForeColor="#330099" BackColor="White" />
       <ItemTemplate>
            <table border="1" style="height:300px;margin-top:30px;margin-left:-0.5px;border:none">
                 <tr style="visibility:hidden">
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label10" runat="server" Text="ID"></asp:Label>
                    </td>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label11" runat="server" style="justify-content:center" Text='<%# Eval("product_id") %>'></asp:Label>
                        
                    </td>
                </tr>
                <tr>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label5" runat="server" Text="Name"></asp:Label>
                    </td>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label1" runat="server" style="justify-content:center" Text='<%# Eval("product_name") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label6" runat="server" Text="Image"></asp:Label>
                    </td>
                    <td style="width: 400px;text-align: center;">
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("product_image_1") %>' Width="130px" Height="80px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label7" runat="server"  Text="Bought Date"></asp:Label>
                    </td>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label2" runat="server"  Text='<%# Eval("product_bought_date") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label8" runat="server" Text="Sell Date"></asp:Label>
                    </td>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("product_sell_date") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label9" runat="server" Text="Price"></asp:Label>
                    </td>
                    <td style="width: 400px;text-align: center;">
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("product_price") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Read More" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </ItemTemplate>
        <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
    </asp:DataList>
       <asp:Label ID="Labeldt" runat="server" Text=""></asp:Label>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Live Auction %>" SelectCommand="SELECT  [product_id], [product_name], [product_bought_date], [product_sell_date], [product_price], [product_sell_time], [product_image_1] FROM [product_details] WHERE product_sell_date > CURRENT_TIMESTAMP"></asp:SqlDataSource>
        </div>
</asp:Content>
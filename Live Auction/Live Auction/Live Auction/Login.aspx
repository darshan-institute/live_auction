﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Live_Auction.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="logincss.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="login" runat="server">
        <div class="maincontent">

           <%-- <div class="image"> 
                <asp:Image ID="Image1" runat="server" Height="141px" Width="354px" ImageUrl="https://thumbs.dreamstime.com/t/auction-icons-1443193.jpg" />
            </div>--%>

            <div class="headlogin">
                
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Italic="True" Font-Names="Stencil Std" Font-Overline="False" Font-Size="Larger" Font-Underline="False" ForeColor="#805C3C" Height="37px" Text="Live Auction" Width="232px"></asp:Label>
                
            </div>

            <div class="username">
                <asp:Label ID="lblusername" runat="server" Text="Username : " ForeColor="#003300" Font-Bold="True" Font-Italic="True"></asp:Label><br/>
                <asp:TextBox ID="txtusername" runat="server" Width="215px"></asp:TextBox><br />
                <asp:Label ID="lblpassword" runat="server" Text="Password : " ForeColor="Black" Font-Bold="True" Font-Italic="True"></asp:Label><br/>
                  <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" Width="215px"></asp:TextBox>
                
            </div>

            <div class="loginbutton">
                <asp:Button ID="loginbutton" runat="server" Text="Login" width = "110px" BackColor="#DAAE87" BorderColor="Black" BorderStyle="Outset" Font-Bold="True" Font-Italic="False" Font-Strikeout="False" OnClick="loginbutton_Click"/>
                
                <asp:Button ID="register" runat="server" Text="Sign Up" width = "110px" BackColor="#DAAE87" BorderColor="Black" BorderStyle="Outset" Font-Bold="True" Font-Italic="False" Font-Strikeout="False" OnClick="register_Click"/>
                
            </div>
            <div class="errormsg">
                <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
            </div>
            
         </div>
    </form>
</body>
</html>

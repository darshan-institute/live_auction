﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="signup.aspx.cs" Inherits="Live_Auction.signup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="logincss.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="maincontent">

            <%--<div class="image"> 
                    <asp:Image ID="Image1" runat="server" Height="141px" Width="354px" ImageUrl="https://thumbs.dreamstime.com/t/auction-icons-1443193.jpg" />
            </div>--%>
            <div class="headsignup">
                
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" Font-Names="Stencil Std" Font-Overline="False" Font-Size="Larger" Font-Underline="False" ForeColor="#805C3C" Height="37px" Text="Live Auction" Width="232px"></asp:Label>
                
            </div>

            <div class="signup">
                
                
                <asp:Label ID="lblfirstname" runat="server" Text="First Name" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon1" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtfirstname" runat="server" Width="215px"></asp:TextBox>
                <asp:Label ID="lblfirstnameerrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br />
                

                <asp:Label ID="lbllastname" runat="server" Text="Last Name" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon2" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtlastname" runat="server" Width="215px"></asp:TextBox>
                 <asp:Label ID="lbllastnameerrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br />
                
                <asp:Label ID="lblusername" runat="server" Text="Username" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon3" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtusername" runat="server" Width="215px"></asp:TextBox>
                 <asp:Label ID="lblusernameerrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br/>
                
                <asp:Label ID="lblpassword" runat="server" Text="Password" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon4" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" Width="215px"></asp:TextBox>
                 <asp:Label ID="lblpassworderrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br/>
                
                <asp:Label ID="lblconfirmpass" runat="server" Text="Confirm Password" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon5" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtconfirmpass" runat="server" TextMode="Password" Width="215px"></asp:TextBox>
                <asp:Label ID="lblconfirmpasserrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br/>
                
                <asp:Label ID="lblgender" runat="server" Text="Gender" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon6" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:RadioButton ID="male" runat="server" text="Male" OnCheckedChanged="male_CheckedChanged" />
                <asp:RadioButton ID="female" runat="server" text="Female" OnCheckedChanged="female_CheckedChanged"/>
                 <asp:Label ID="lblgendererrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br />
                
                
                <asp:Label ID="lbladdress" runat="server" Text="Address" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon7" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtaddress" runat="server" Width="215px" TextMode="MultiLine"></asp:TextBox>
                <asp:Label ID="lbladdresserrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br/>
                

                <asp:Label ID="lblemail" runat="server" Text="Email" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon8" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtemail" runat="server" Width="215px"></asp:TextBox>
                <asp:Label ID="lblemailerrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br/>
                
                <asp:Label ID="lblcontactno" runat="server" Text="Contact No" Width="160px"></asp:Label>
                <asp:Label ID="lblcolon9" runat="server" Text=":" Width="30px"></asp:Label>
                <asp:TextBox ID="txtcontactno" runat="server" TextMode="Number" Width="215px"></asp:TextBox>
                <asp:Label ID="lblcontactnoerrormsg" runat="server" ForeColor="Red"></asp:Label>
                <br/>

                

               

            </div>

            <div class="loginbutton" >
            <asp:Button ID="register" runat="server" Text="Register" width = "110px" BackColor="#DAAE87" BorderColor="Black" BorderStyle="Outset" Font-Bold="True" Font-Italic="False" Font-Strikeout="False" OnClick="register_Click" />
            </div>

       </div>
    </form>
</body>
</html>

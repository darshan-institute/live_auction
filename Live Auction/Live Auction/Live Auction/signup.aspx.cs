﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;

namespace Live_Auction
{
    public partial class signup : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        string gender;
        protected void register_Click(object sender, EventArgs e)
        {
            if (txtfirstname.Text == "")
            {
                lblfirstnameerrormsg.Text = "Enter FirstName...";
            }
            else if (txtlastname.Text == "")
            {
                lbllastnameerrormsg.Text = "Enter LastName...";
            }
            else if (txtusername.Text == "")
            {
                lblusernameerrormsg.Text = "Enter UserName...";
            }
            else if (txtpassword.Text == "")
            {
                lblpassworderrormsg.Text = "Enter Password...";
            }
            else if (txtpassword.Text == txtconfirmpass.Text)
            {
                lblconfirmpasserrormsg.Text = "Password Doesn't Match";
            }
            else if (txtaddress.Text == "")
            {
                lbladdresserrormsg.Text = "Enter Address...";
            }
            else if (txtemail.Text == "")
            {
                lblemailerrormsg.Text = "Enter Email...";
            }
            else if (txtcontactno.Text == "")
            {
                lblcontactnoerrormsg.Text = "Enter ContactNo...";
            }
            else
            {
                String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                con.ConnectionString = constr;
                con.Open();
                try
                {
                    string insertdata = "INSERT INTO signup(first_name,last_name,user_name,password,gender,address,email,contact_no) values " +
                        "('" + txtfirstname.Text + "','" + txtlastname.Text + "','" + txtusername.Text + "','" + txtpassword.Text + "','" + gender + "', '" + txtaddress.Text + "','" + txtemail.Text + "','" + txtcontactno.Text + "')";
                    SqlCommand cmd = new SqlCommand(insertdata, con);
                    cmd.ExecuteNonQuery();
                    string msg = "";
                    msg = "Register Successfully..!!";
                    ClientScript.RegisterStartupScript(this.GetType(), "", "alert('" + msg + "');", true);
                }
                catch (Exception)
                {
                    string error = "Username already exist";
                    ClientScript.RegisterStartupScript(this.GetType(), "", "alert('" + error + "');", true);
                }
                finally
                {
                    txtusername.Text = " ";
                }

                con.Close();
                //Response.Redirect("login.aspx");
            }
        }
        
        protected void male_CheckedChanged(object sender, EventArgs e)
        {
            gender = "male";
        }

        protected void female_CheckedChanged(object sender, EventArgs e)
        {
            gender = "female";
        }
    }
}
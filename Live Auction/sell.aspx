﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sell.aspx.cs" MasterPageFile="~/Masterpage.Master" Inherits="Live_Auction.sell" %>


<asp:Content ID="cnt0" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="cnt1" ContentPlaceHolderID="contentbody" runat="server">
   
   <script>
      
   </script>

        <div>
        <div style="text-align : center;font-size:35px;" >
            <asp:Label ID="lblheadsell" runat="server" Text="Enter Product's Details" ForeColor="Purple" Font-Bold="true" CssClass="input100"></asp:Label> 
        </div>


            <div style="margin-top:20px;" >

                <div >
                    <asp:Table runat="server"  Width="900px" style="text-align:left;margin-left:180px;">
                        <asp:TableRow style="height: 50px;background-color: blueviolet;color: white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="lblproductname"  runat="server" Text="Product's Name"  Style="margin-top:20px;"  Font-Size="25px"></asp:Label></asp:TableCell>
                            <asp:TableCell style="padding-right:80px">:</asp:TableCell>
                            <asp:TableCell ><asp:TextBox ID="txtproductname" runat="server" BorderStyle="None" Font-Size="20px" AutoCompleteType="Disabled"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                          
                        <asp:TableRow style="height: 50px;background-color:dodgerblue;color:white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="lblcalboughtdate"  runat="server" Style="padding-top:30px;"  Text="Product's Bought Date"  Font-Size="25px"></asp:Label></asp:TableCell>
                            <asp:TableCell>:</asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtboughtdate" runat="server" TextMode="Date" Style="margin-top:10px;" BorderStyle="None" Font-Size="20px"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow style="height: 50px;background-color: blueviolet;color: white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="lblprice" runat="server" Style="margin-top:10px;" Text="Product's Price" Font-Size="25px"></asp:Label></asp:TableCell>
                            <asp:TableCell>:</asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtprice" runat="server" Style="margin-top:10px;"  BorderStyle="None" TextMode ="Number" AutoCompleteType="Disabled" Font-Size="20px"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                       
                        

                        <asp:TableRow style="height: 50px;background-color:dodgerblue;color:white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="lblselldate" runat="server" Text="Product's Sell Date"   Font-Size="25px"></asp:Label></asp:TableCell>
                            <asp:TableCell>:</asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtselldate" runat="server" BorderStyle="None" TextMode="Date" Font-Size="20px"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow style="height: 50px;background-color: blueviolet;color: white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="lblselltime" runat="server" Text="Product's Sell Time"   Font-Size="25px"></asp:Label></asp:TableCell>
                           <asp:TableCell>:</asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtselltime" runat="server" BorderStyle="None" TextMode="Time" Format="hh:mm:ss" Font-Size="20px"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        
                        <asp:TableRow style="height: 50px;background-color:dodgerblue;color:white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="Label1" runat="server" Text="Insert Product's Image 1"   Font-Size="25px"></asp:Label> </asp:TableCell>
                            <asp:TableCell>:</asp:TableCell>
                            <asp:TableCell><asp:FileUpload ID="image1" runat="server" BorderStyle="None" Font-Size="20px"/></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow style="height: 50px;background-color: blueviolet;color: white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="Label2" runat="server" Text="Insert Product's Image 2"  Font-Size="25px"></asp:Label></asp:TableCell>
                            <asp:TableCell>:</asp:TableCell>
                            <asp:TableCell><asp:FileUpload ID="FileUpload2" runat="server" BorderStyle="None" Font-Size="20px"/></asp:TableCell>
                        </asp:TableRow>

                         <asp:TableRow style="height: 50px;background-color:dodgerblue;color:white;font-family: 'Times New Roman', Times, serif;">
                            <asp:TableCell style="padding-left:120px"><asp:Label ID="Label3" runat="server" Text="Insert Product's Image 3"   Font-Size="25px"></asp:Label></asp:TableCell>
                             <asp:TableCell>:</asp:TableCell>
                             <asp:TableCell><asp:FileUpload ID="FileUpload3" runat="server" BorderStyle="None" Font-Size="20px"/></asp:TableCell>
                        </asp:TableRow>

                        
                    </asp:Table>
                   <asp:Button ID="imgbtn"  runat="server" OnClick="imgbtn_Click" Text="Submit" style="float:right;margin-right:140px;background:linear-gradient(aqua,lightblue);margin-top:10px;" BorderStyle="none" Font-Size="30px"/>
                     
                </div>
                </div>
            </div>
</asp:Content>



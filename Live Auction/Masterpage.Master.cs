﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;

namespace Live_Auction
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        SqlConnection con = new SqlConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["Username"] == null)
            {
                ////lsbtn.Visible = true;
            }
            else
            {
                //lsbtn.Visible = false;
                username.Text = Session["Username"].ToString();
                //lblusername1.Text = Session["Username"].ToString();
            }
            String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            con.ConnectionString = constr;
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT * FROM user_detail", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteReader();

            //lblusername.Text=Session["UserName"].ToString();

        }

        protected void lsbtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
    }
}
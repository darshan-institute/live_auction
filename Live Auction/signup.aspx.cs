﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;

namespace Live_Auction
{
    public partial class signup : System.Web.UI.Page
    {

        SqlConnection con = new SqlConnection();
        string gender;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rdbmale_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Male";

        }

        protected void rdbfemale_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Female";


        }




        protected void register_Click(object sender, EventArgs e)
        {



            if (txtfirstname.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter First Name');", true);

            }
            else if (txtlastname.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Last Name');", true);
            }
            else if (txtusername.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Username');", true);
            }
            else if (txtpassword.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Password');", true);
            }
            else if (txtcnpassword.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Confirm Password');", true);
            }

            else if (txtaddress.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Address');", true);
            }
            else if (txtemail.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Email');", true);
            }
            else if (txtcontactno.Text == "")
            {

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Number');", true);
            }
            else if (txtcontactno.Text.Length > 10 || txtcontactno.Text.Length < 10)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Valid Number');", true);
            }
            else if (rdbmale.Checked == false && rdbfemale.Checked == false)
            {
               
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Select Your Gender..');", true);
            }
            else if (txtpassword.Text != txtcnpassword.Text)
            {
                lblconfirmpasserrormsg.Text = "Password Doesn't Match";
            }
            else
                {

                String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                con.ConnectionString = constr;
                con.Open();
                try
                {
                    string insertdata = "INSERT INTO user_detail(user_first_name,user_last_name,user_name,user_password,user_gender,user_address,user_email,user_contact_no) values " +
                     "('" + txtfirstname.Text + "','" + txtlastname.Text + "','" + txtusername.Text + "','" + txtpassword.Text + "','" + gender + "', '" + txtaddress.Text + "','" + txtemail.Text + "','" + txtcontactno.Text + "')";
                    SqlCommand cmd = new SqlCommand(insertdata, con);
                    cmd.ExecuteNonQuery();
                    string msg = "";
                    msg = "Register Successfully..!!";
                    ClientScript.RegisterStartupScript(this.GetType(), "", "alert('" + msg + "');", true);


                }
                catch (System.Data.SqlClient.SqlException )
                {
                string error = "Username already exist";
                ClientScript.RegisterStartupScript(this.GetType(), "", "alert('" + error + "');", true);
            }
            

            con.Close();
                //Response.Redirect("login.aspx");
            }
        }

       
    }
}
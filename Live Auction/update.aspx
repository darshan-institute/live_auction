﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" MasterPageFile="~/Masterpage.Master" Inherits="Live_Auction.update" %>

<asp:Content ID="cnt0" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="cnt1" ContentPlaceHolderID="contentbody" runat="server">

   <%-- <script>
        <link rel="stylesheet" type="text/css" href="css/util.css" />
        <link rel="stylesheet" type="text/css" href="css/main.css" />
    </script>--%>

        <div>
        <div style="text-align : center;font-size:35px;" >
            <asp:Label ID="lblheadsell" runat="server" Text="Enter Product's Details" ForeColor="Purple" Font-Bold="true" CssClass="input100"></asp:Label> 
        </div>


            <div style="margin-top:20px;" >

                <div >
                    <asp:Table runat="server" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblproductname" runat="server" Text="Product's Name : " Style="margin-top:20px;"></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtproductname" runat="server" BorderStyle="None"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                          
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblcalboughtdate"  runat="server" Style="padding-top:30px;" Text="Product's Bought Date : "></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtboughtdate" runat="server" Style="margin-top:10px;"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblprice" runat="server" Style="margin-top:10px;" Text="Product's Price : "></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtprice" runat="server" Style="margin-top:10px;" CssClass="hasDatepicker" TextMode="Number"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                       
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblproducttype" runat="server" Text="Product's Type : "></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:DropDownList ID="ddlproducttype" runat="server"></asp:DropDownList></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblselldate" runat="server" Text="Product's Sell Date : "></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:TextBox ID="txtselldate" runat="server" ></asp:TextBox></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="Label1" runat="server" Text="Insert Product's Image 1:"></asp:Label> </asp:TableCell>
                            <asp:TableCell><asp:FileUpload ID="image1" runat="server" /></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="Label2" runat="server" Text="Insert Product's Image 2:"></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:FileUpload ID="FileUpload2" runat="server" /></asp:TableCell>
                        </asp:TableRow>

                         <asp:TableRow>
                            <asp:TableCell><asp:Label ID="Label3" runat="server" Text="Insert Product's Image 3:"></asp:Label></asp:TableCell>
                             <asp:TableCell><asp:FileUpload ID="FileUpload3" runat="server" /></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell> <asp:Button ID="imgbtn" runat="server" OnClick="imgbtn_Click" Text="Submit" style="background:linear-gradient(lightpink,lightblue);margin-top:10px;" BorderStyle="none"/></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
                </div>
            </div>
</asp:Content>

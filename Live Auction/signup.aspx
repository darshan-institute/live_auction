﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="signup.aspx.cs" Inherits="Live_Auction.signup" %>



<!DOCTYPE html>

<html lang="en">
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bgimage.png')">
			<div class="wrap-login100" style="width:45%;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
				<form class="login100-form validate-form" runat="server">
					
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Sign Up
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter Firstname">
                        <asp:TextBox ID="txtfirstname" runat="server" CssClass="input100" placeholder="Firstname" AutoCompleteType="Disabled"></asp:TextBox>
						
						<span class="focus-input100"></span>
						
					
		
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Enter Lastname">
                        <asp:TextBox ID="txtlastname" runat="server" CssClass="input100" placeholder="Lastname" AutoCompleteType="Disabled"></asp:TextBox>
						<span class="focus-input100" ></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
                        <asp:TextBox ID="txtusername" runat="server" CssClass="input100" placeholder="Username" AutoCompleteType="Disabled"></asp:TextBox>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<asp:TextBox ID="txtpassword" runat="server" CssClass="input100" placeholder="Password" TextMode="Password"></asp:TextBox>
						<span class="focus-input100" ></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter Confirmpassword">
						<asp:TextBox ID="txtcnpassword" runat="server" CssClass="input100" placeholder="Confirm Password" TextMode="Password"></asp:TextBox>
						<span class="focus-input100" ></span>
					</div>
					<asp:Label ID="lblconfirmpasserrormsg" runat="server" Text="" style="color:red;"></asp:Label>
					
					
					<div class="wrap-input100 validate-input" data-validate = "Enter Address">
                        <asp:TextBox ID="txtaddress" runat="server" CssClass="input100" placeholder="Address" TextMode="MultiLine"></asp:TextBox>
						<span class="focus-input100" ></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Enter Email">
                        <asp:TextBox ID="txtemail" runat="server" CssClass="input100" placeholder="Email" TextMode="Email" AutoCompleteType="Disabled"></asp:TextBox>
						<i class="focus-input100" ></i>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Enter Number">
                        <asp:TextBox ID="txtcontactno" runat="server" TextMode="Number" CssClass="input100" placeholder="Number" AutoCompleteType="Disabled"></asp:TextBox>
						<span class="focus-input100" ></span>
					</div>

					<div style="margin-left:45px;margin-bottom:30px;color:white;font-size:20px;">
						<label id="lblgender" runat="server" style="margin-bottom:-4%;">Gender : </label>

						<asp:RadioButton ID="rdbmale" Name="male" runat="server" GroupName="Gender" style="margin-left:150px;" OnCheckedChanged="rdbmale_CheckedChanged" />
						<asp:Label ID="lblmale" style="margin-left:2%" runat="server"  Text="Male"></asp:Label>

                        <asp:RadioButton ID="rdbfemale" Name="male" runat="server" GroupName="Gender" style="margin-left:4%" OnCheckedChanged="rdbfemale_CheckedChanged"  />
						<asp:Label ID="Label1" runat="server" style="margin-left:2%" Text="Female"></asp:Label>
		
					</div>

					
					
					<asp:Label ID="lblgendererrormsg" runat="server" Text="" style="color:red;"></asp:Label>

					<div class="container-login100-form-btn">
						<button id="register" runat="server" class="login100-form-btn" onserverclick="register_Click"> 
							Register</button>
						
					</div>
					
                    
					
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>

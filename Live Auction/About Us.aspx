﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About Us.aspx.cs" Inherits="Live_Auction.displayhomepage" %>

<!DOCTYPE HTML>

<html>
	<head>
		<title>Live Auction</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a class="logo" href="#"></a>
					
				<nav>
					<a href="#menu">Menu</a>
				</nav>
			</header>

		
			<nav id="menu">
				
				<ul class="links">
					<li><a href="login.aspx">Login</a></li>
					<li><a href="signup.aspx">Signup</a></li>
				</ul>
			</nav>

	
			<section id="banner">
				<div class="inner">
					<h1>Live Auction</h1>
				</div>
				<video autoplay loop muted  src="images/video.mp4"></video>
			</section>

		
			<section class="wrapper">
			
				<div class="highlights">	
							<div class="content" style="margin-left:500px;">
								<header>
									<a href="home.aspx" class="icon fa-qrcode"><span class="label">Icon</span></a>
									<h3>View Products</h3>
								</header>
								<p>View Products , Choose Product , Buy product or Sell Product</p>
							</div>
					</div>
				

				
			</section>

		
			<section id="cta" class="wrapper">
				<div class="inner">
					<h1>BID AS MUCH AS YOU CAN</h1>
					</div>
			</section>

		

	
			<footer id="footer">
				<div class="inner">
					<div class="content">
						<section>
							<h2>About Us</h2>
							<p>We are students of Darshan Institute of Engg. and Technology , We Designed a Project Live auction to Buy or sell Products on the highest bids.</p>
						</section>
						
						<section>
							<h4>Contact Us Here</h4>
							<ul class="plain">
								<li><a href="#"><i class="icon fa-twitter">&nbsp;</i>Twitter</a></li>
								<li><a href="#"><i class="icon fa-facebook">&nbsp;</i>Facebook</a></li>
								<li><a href="#"><i class="icon fa-instagram">&nbsp;</i>Instagram</a></li>
								
							</ul>
						</section>
					</div>
					<div class="copyright">
						<h4 style="color:ghostwhite;">Copyright to Darshan Institute of Engg. and Technology</h4>
					</div>
				</div>
			</footer>

		
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>


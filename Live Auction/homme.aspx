﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="homme.aspx.cs" Inherits="Live_Auction.homme" %>


<head>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 266px;
        }
        .auto-style4 {
            width: 400px;
            text-align: center;
        }
        .auto-style5 {
            text-align: center;
        }
    </style>
</head>



<form id="form1" runat="server">
    <asp:DataList ID="DataList1" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None"  BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource1" GridLines="Both" style="margin-right: 513px" CellSpacing="2" RepeatDirection="Horizontal">
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
        <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <ItemTemplate>
            <table border="1" class="auto-style1">
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label5" runat="server" Text="Name"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:Label ID="Label1" runat="server" style="justify-content:center" Text='<%# Eval("product_name") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label6" runat="server" Text="Image"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("product_image_1") %>' Width="130px" Height="80px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label7" runat="server"  Text="Bought Date"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:Label ID="Label2" runat="server"  Text='<%# Eval("product_bought_date") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label8" runat="server" Text="Sell Date"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("product_sell_date") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label9" runat="server" Text="Price"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("product_price") %>'></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </ItemTemplate>
        <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
    </asp:DataList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Live Auction %>" SelectCommand="SELECT [product_name], [product_bought_date], [product_sell_date], [product_price], [product_sell_time], [product_image_1] FROM [product_details]"></asp:SqlDataSource>
</form>




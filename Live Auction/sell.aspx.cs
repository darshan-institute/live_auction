﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;
using System.IO;

namespace Live_Auction
{
    public partial class sell : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserName"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            con.ConnectionString = constr;
            con.Open();
            string proqry = "Select * from product_type_master";
            SqlCommand cmd = new SqlCommand(proqry, con);
            cmd.ExecuteNonQuery();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);  // fill dataset 

        


        }

        

        protected void imgbtn_Click(object sender, EventArgs e)
        {

            
            SqlConnection con = new SqlConnection();/*("Data Source=.;Initial Catalog=Vprs;Integrated Security=True");*/
            String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            con.ConnectionString = constr;
            con.Open();
          
            string path = Server.MapPath("Images/");

            if (image1.HasFile)
            {
                if(FileUpload3.HasFile && FileUpload2.HasFile)
                {
                    string ext = Path.GetExtension(image1.FileName);
                    string ext1 = Path.GetExtension(FileUpload2.FileName);
                    string ext2 = Path.GetExtension(FileUpload3.FileName);

                    if ((ext == ".jpg" || ext == ".jpeg" || ext == ".png") && (ext1 == ".jpg" || ext1 == ".jpeg" || ext1 == ".png") && (ext2 == ".jpg" || ext2 == ".jpeg" || ext2 == ".png") )
                    {
                        string name,name1,name2;
                        image1.SaveAs(path + image1.FileName);
                        name = "Images/" + image1.FileName;
                        FileUpload2.SaveAs(path + FileUpload2.FileName);
                        name1 = "Images/" + FileUpload2.FileName;
                        FileUpload3.SaveAs(path + FileUpload3.FileName);
                        name2 = "Images/" + FileUpload3.FileName;
                        
                        string proqry = "insert into  product_details(product_name,user_name,product_bought_date,product_sell_Date,product_sell_time,product_price,product_image_1,product_image_2,product_image_3) values('" + txtproductname.Text + "','" + Session["UserName"].ToString() + "','" + txtboughtdate.Text + "','" + txtselldate.Text + "','" + txtselltime.Text + "','" + txtprice.Text + "','" + name + "','" + name1 + "','" + name2 + "')";
                        
                        SqlCommand cmd = new SqlCommand(proqry, con);
                        cmd.ExecuteNonQuery();
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Product Add Succesfully');", true);
                    }
                }
                else if(FileUpload2.HasFile)
                {
                    string ext = Path.GetExtension(image1.FileName);
                    string ext1 = Path.GetExtension(FileUpload2.FileName);
                    if ((ext == ".jpg" || ext == ".jpeg" || ext == ".png") && (ext1 == ".jpg" || ext1 == ".jpeg" || ext1 == ".png"))
                    {
                        string name, name1;
                        image1.SaveAs(path + image1.FileName);
                        name = "Images/" + image1.FileName;
                        FileUpload2.SaveAs(path + FileUpload2.FileName);
                        name1 = "Images/" + FileUpload2.FileName;
                        
                        string proqry = "insert into  product_details(product_name,user_name,product_bought_date,product_sell_Date,product_sell_time,product_price,product_image_1,product_image_2) values('" + txtproductname.Text + "','" + Session["UserName"].ToString() + "','" + txtboughtdate.Text + "','" + txtselldate.Text + "','" + txtselltime.Text + "','" + txtprice.Text + "','" + name + "','" + name1 + "')";
                        SqlCommand cmd = new SqlCommand(proqry, con);
                        cmd.ExecuteNonQuery();
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Product Add Succesfully');", true);
                    }
                }
                else
                {
                    string name;
                    string ext = Path.GetExtension(image1.FileName);
                    if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
                    {
                        image1.SaveAs(path + image1.FileName);
                        name = "Images/" + image1.FileName;
                        string proqry = "insert into  product_details(product_name,user_name,product_bought_date,product_sell_Date,product_sell_time,product_price,product_image_1) values('" + txtproductname.Text + "','"+Session["UserName"].ToString()+"','" + txtboughtdate.Text + "','" + txtselldate.Text + "','"+ txtselltime.Text +"','" + txtprice.Text + "','" + name + "')";
                        SqlCommand cmd = new SqlCommand(proqry, con);
                        cmd.ExecuteNonQuery();
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Product Add Succesfully');", true);
                    }
                }
            }
        }
    }
}

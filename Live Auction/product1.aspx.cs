﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;
using System.IO;


namespace Live_Auction
{
    public partial class product1 : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection();
        
        protected void Page_Load(object sender, EventArgs e)
        {
             if(Session["UserName"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else 
            {
                if (!IsPostBack)
                {
                    displaydata();
                }      
            }

        }

        public void displaydata()
        {
            String myQuery = String.Format("SELECT * FROM  product_details where user_name ='"+Session["UserName"].ToString()+"'");
            
            //SqlDataSourceDeviceID.DataBind();
            GridProduct.DataSource = SqlDataSource1;
            SqlDataSource1.SelectCommand = myQuery;
            GridProduct.DataBind();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridProduct.EditIndex = e.NewEditIndex;
            displaydata();
            GridProduct.EditRowStyle.BackColor = System.Drawing.Color.Cyan;
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridProduct.EditIndex = -1;
            displaydata();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            con.ConnectionString = constr;
            con.Open();

          
           
            Label id = GridProduct.Rows[e.RowIndex].FindControl("lblproid") as Label;
            TextBox productname = GridProduct.Rows[e.RowIndex].FindControl("txtproductname") as TextBox;
            TextBox productselltime = GridProduct.Rows[e.RowIndex].FindControl("txtproductselltime") as TextBox;
            TextBox productprice = GridProduct.Rows[e.RowIndex].FindControl("txtproductprice") as TextBox;
            TextBox selldate = GridProduct.Rows[e.RowIndex].FindControl("txtproductselldate") as TextBox; 
            TextBox boughtdate = GridProduct.Rows[e.RowIndex].FindControl("txtproductboughtdate") as TextBox;
            boughtdate.TextMode = TextBoxMode.Date;
            selldate.TextMode = TextBoxMode.Date;
            productselltime.TextMode = TextBoxMode.Time;
            FileUpload FileUpload1 = (FileUpload) GridProduct.Rows[e.RowIndex].FindControl("imageupdate");
            FileUpload FileUpload2 = (FileUpload)GridProduct.Rows[e.RowIndex].FindControl("imageupdate2");
            FileUpload FileUpload3 = (FileUpload)GridProduct.Rows[e.RowIndex].FindControl("imageupdate3");
          
            string path = Server.MapPath("Images/");

            if (FileUpload1.HasFile)
            {
                string name;
                string ext = Path.GetExtension(FileUpload1.FileName);
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
                {
                    FileUpload1.SaveAs(path + FileUpload1.FileName);
                    name = "Images/" + FileUpload1.FileName;
                    String qry = "UPDATE product_details set product_name='" + productname.Text + "' , product_bought_date='" + boughtdate.Text + "',product_sell_date='" + selldate.Text + "',product_sell_time ='" + productselltime.Text + "',product_price='" + productprice.Text + "',product_image_1='"+name+"' WHERE product_id='" + id.Text + "'";
                    SqlCommand cmd = new SqlCommand(qry, con);
                    cmd.ExecuteNonQuery();
                    Response.Redirect("product1.aspx");
                   
                }
                
                

            }
            if (FileUpload2.HasFile)
            {
                string name1;
                string ext1 = Path.GetExtension(FileUpload2.FileName);
                if (ext1 == ".jpg" || ext1 == ".jpeg" || ext1 == ".png")
                {

                    FileUpload2.SaveAs(path + FileUpload2.FileName);
                    name1 = "Images/" + FileUpload2.FileName;
                    String qry = "UPDATE product_details set product_name='" + productname.Text + "' , product_bought_date='" + boughtdate.Text + "',product_sell_date='" + selldate.Text + "',product_sell_time ='" + productselltime.Text + "',product_price='" + productprice.Text + "',product_image_2='" + name1 + "' WHERE product_id='" + id.Text + "'";
                    SqlCommand cmd = new SqlCommand(qry, con);
                    cmd.ExecuteNonQuery();
                    Response.Redirect("product1.aspx");

                }
            }
            if (FileUpload3.HasFile)
            {
                string name2;
                string ext2 = Path.GetExtension(FileUpload3.FileName);
                if (ext2 == ".jpg" || ext2 == ".jpeg" || ext2 == ".png")
                {

                    FileUpload3.SaveAs(path + FileUpload3.FileName);
                    name2 = "Images/" + FileUpload3.FileName;
                    String qry = "UPDATE product_details set product_name='" + productname.Text + "' , product_bought_date='" + boughtdate.Text + "',product_sell_date='" + selldate.Text + "',product_sell_time ='" + productselltime.Text + "',product_price='" + productprice.Text + "',product_image_3='" + name2 + "' WHERE product_id='" + id.Text + "'";
                    SqlCommand cmd = new SqlCommand(qry, con);
                    cmd.ExecuteNonQuery();
                    Response.Redirect("product1.aspx");

                }
            }
            else
            {
                String qry = "UPDATE product_details set product_name='" + productname.Text + "' , product_bought_date='" + boughtdate.Text + "',product_sell_date='" + selldate.Text + "',product_sell_time ='" + productselltime.Text + "',product_price='" + productprice.Text + "' WHERE product_id='" + id.Text + "'";
                SqlCommand cmd = new SqlCommand(qry, con);
                cmd.ExecuteNonQuery();
                Response.Redirect("product1.aspx");
            }









        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            String constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            con.ConnectionString = constr;
            con.Open();
            Label id = GridProduct.Rows[e.RowIndex].FindControl("lblprodid") as Label;
            string pr = "DELETE FROM product_details WHERE product_id = '"+ id.Text +"' ";
            SqlCommand cmd = new SqlCommand(pr, con);
            cmd.ExecuteNonQuery();
            displaydata();

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="product1.aspx.cs" MasterPageFile="~/Masterpage.Master" Inherits="Live_Auction.product1" %>


<asp:Content ID="cnt0" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="cnt1" ContentPlaceHolderID="contentbody" runat="server">

    <div style="margin-left:-80px">
    <asp:GridView ID="GridProduct"  runat="server"  AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" Width="776px">
        <Columns>
            <asp:TemplateField HeaderText="ID" Visible="false" >
                <EditItemTemplate >
                    <asp:Label ID="lblproid" runat="server" Text='<%# Eval("product_id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblprodid" runat="server" Text='<%# Eval("product_id") %>'></asp:Label>
                      
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name" ControlStyle-Width="100px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtproductname" runat="server" Text='<%# Eval("product_name") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbproductname" runat="server" style="text-align: center" Text='<%# Eval("product_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Bought Date" ControlStyle-Width="100px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtproductboughtdate" runat="server"  Text='<%# Eval("product_bought_date") %>'  TextMode="Date"></asp:TextBox>
                    
                </EditItemTemplate>
                <ItemTemplate>
                   
                    <asp:Label ID="lbproductboughtdate" runat="server" Text='<%# Eval("product_bought_date") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sell Date" ControlStyle-Width="100px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtproductselldate" runat="server" Text='<%# Eval("product_sell_date") %>' placeholder='<%# Eval("product_sell_date") %>' TextMode="Date"></asp:TextBox>
                   
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbproductselldate" runat="server" Text='<%# Eval("product_sell_date") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sell Time" ControlStyle-Width="100px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtproductselltime" runat="server" Text='<%# Eval("product_sell_time") %>' TextMode="Time" Format="hh:mm:ss"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblproductselltime" runat="server" Text='<%# Eval("product_sell_time") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Price" ControlStyle-Width="100px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtproductprice" runat="server" Text='<%# Eval("product_price") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblproductprice" runat="server" Text='<%# Eval("product_price") %>'></asp:Label>
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Image" ControlStyle-Width="170px">
                <EditItemTemplate>
                    <asp:Image ImageUrl='<%# Eval("product_image_1") %>' width="50px" runat="server"/>
                    <asp:FileUpload ID="imageupdate" runat="server" BorderStyle="None" Font-Size="20px"/>
                </EditItemTemplate>
                <ItemTemplate>
                  <asp:Image ImageUrl='<%# Eval("product_image_1") %>' width="50px" runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Image" ControlStyle-Width="170px">
                <EditItemTemplate>
                    <asp:Image ImageUrl='<%# Eval("product_image_2") %>' width="50px" runat="server"/>
                    <asp:FileUpload ID="imageupdate2" runat="server" BorderStyle="None" Font-Size="20px"/>
                </EditItemTemplate>
                <ItemTemplate>
                  <asp:Image ImageUrl='<%# Eval("product_image_2") %>' width="50px" runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Image" ControlStyle-Width="170px">
                <EditItemTemplate>
                    <asp:Image ImageUrl='<%# Eval("product_image_3") %>' width="50px" runat="server"/>
                    <asp:FileUpload ID="imageupdate3" runat="server" BorderStyle="None" Font-Size="20px"/>
                </EditItemTemplate>
                <ItemTemplate>
                  <asp:Image ImageUrl='<%# Eval("product_image_3") %>' width="50px" runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderText="Operations">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="update">Update</asp:LinkButton>
                    
                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
              
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="edit">Edit</asp:LinkButton>
                    
                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="delete">Detele</asp:LinkButton>
                </ItemTemplate>
                <ControlStyle Width="80px" />
                <FooterStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FFF1D4" />
        <SortedAscendingHeaderStyle BackColor="#B95C30" />
        <SortedDescendingCellStyle BackColor="#F1E5CE" />
        <SortedDescendingHeaderStyle BackColor="#93451F" />
    </asp:GridView>

        </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Live Auction %>" SelectCommand="SELECT product_id, product_name, user_name, product_type_id, product_bought_date, product_type, product_sell_date, product_sell_time, product_price, product_image_1, product_image_2, product_image_3 FROM product_details"></asp:SqlDataSource>

    </asp:Content>



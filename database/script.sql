USE [master]
GO
/****** Object:  Database [Live Auction]    Script Date: 15-01-2021 15:31:17 ******/
CREATE DATABASE [Live Auction]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'6SEM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\6SEM.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'6SEM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\6SEM_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Live Auction] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Live Auction].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Live Auction] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Live Auction] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Live Auction] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Live Auction] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Live Auction] SET ARITHABORT OFF 
GO
ALTER DATABASE [Live Auction] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Live Auction] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Live Auction] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Live Auction] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Live Auction] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Live Auction] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Live Auction] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Live Auction] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Live Auction] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Live Auction] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Live Auction] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Live Auction] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Live Auction] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Live Auction] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Live Auction] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Live Auction] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Live Auction] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Live Auction] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Live Auction] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Live Auction] SET  MULTI_USER 
GO
ALTER DATABASE [Live Auction] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Live Auction] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Live Auction] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Live Auction] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Live Auction]
GO
/****** Object:  StoredProcedure [dbo].[sp_student_detail_insert]    Script Date: 15-01-2021 15:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_student_detail_insert]
@Stu_Name varchar(50),
@Stu_EmailID varchar(50),
@Stu_ContactNo varchar(50),
@Stu_EnrollmentNo varchar(50)
as
insert into Student_Detail values (@Stu_Name,@Stu_EmailID,@Stu_ContactNo,@Stu_EnrollmentNo)
GO
/****** Object:  Table [dbo].[product_details]    Script Date: 15-01-2021 15:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product_details](
	[product_id] [bigint] IDENTITY(1,1) NOT NULL,
	[product_name] [nvarchar](50) NULL,
	[user_name] [nvarchar](50) NULL,
	[product_type_id] [bigint] NULL,
	[product_bought_date] [nvarchar](50) NULL,
	[product_type] [nchar](10) NULL,
	[product_sell_date] [nvarchar](50) NOT NULL,
	[product_sell_time] [nvarchar](50) NULL,
	[product_price] [bigint] NULL,
	[product_image_1] [nvarchar](max) NULL,
	[product_image_2] [nvarchar](max) NULL,
	[product_image_3] [nvarchar](max) NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[product_type_master]    Script Date: 15-01-2021 15:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product_type_master](
	[product_type_id] [bigint] IDENTITY(1,1) NOT NULL,
	[product_type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_product_type_master] PRIMARY KEY CLUSTERED 
(
	[product_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[transaction_details]    Script Date: 15-01-2021 15:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transaction_details](
	[product_id] [bigint] NOT NULL,
	[user_id] [bigint] NOT NULL,
	[bid_id] [bigint] NOT NULL,
	[user_name] [nvarchar](10) NOT NULL,
	[bid_price] [bigint] NOT NULL,
	[bid_date_time] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user_detail]    Script Date: 15-01-2021 15:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_detail](
	[user_id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_first_name] [nvarchar](50) NOT NULL,
	[user_last_name] [nvarchar](50) NOT NULL,
	[user_name] [nvarchar](50) NOT NULL,
	[user_password] [nvarchar](50) NOT NULL,
	[user_gender] [nvarchar](50) NOT NULL,
	[user_address] [nvarchar](50) NOT NULL,
	[user_email] [nvarchar](50) NOT NULL,
	[user_contact_no] [bigint] NOT NULL,
	[user_balance] [bigint] NULL,
 CONSTRAINT [PK_signup] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [un_name] UNIQUE NONCLUSTERED 
(
	[user_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [unq_username] UNIQUE NONCLUSTERED 
(
	[user_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [Live Auction] SET  READ_WRITE 
GO
